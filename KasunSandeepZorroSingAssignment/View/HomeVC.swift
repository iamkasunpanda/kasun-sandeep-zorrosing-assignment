//
//  HomeVC.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit
import UIView_draggable

class HomeVC: UIViewController {

    var txtDrag: UITextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        view.backgroundColor = .white
        txtDrag.borderStyle = .roundedRect
        txtDrag.placeholder = "Type"
        txtDrag.frame = CGRect(x: 20, y: 100, width: 150, height: 44)
        
        self.view.addSubview(txtDrag)
        
        txtDrag.enableDragging()
    }
    
}
