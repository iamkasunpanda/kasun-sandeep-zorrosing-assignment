//
//  ViewController.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    var lblUserName: UILabel = UILabel()
        var txtUsername: UITextField = UITextField()
        var lblPassword: UILabel = UILabel()
        var txtPassword: UITextField = UITextField()
        var btnLogin: UIButton = UIButton()
        
        var stackContaine: UIStackView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            setupUi()
        }
        
        override func updateViewConstraints() {
            super.updateViewConstraints()
            
            let margins = view.layoutMarginsGuide
            
            stackContaine.translatesAutoresizingMaskIntoConstraints = false
            stackContaine.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20).isActive = true
            stackContaine.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20).isActive = true
            stackContaine.centerYAnchor.constraint(equalTo: margins.centerYAnchor, constant: 0).isActive = true
            
            // Set Login button constraints
            btnLogin.translatesAutoresizingMaskIntoConstraints = false
            btnLogin.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 20).isActive = true
            btnLogin.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -20).isActive = true
            btnLogin.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -20).isActive = true
            let heightConst = NSLayoutConstraint(item: btnLogin, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 44)
            heightConst.isActive = true
            btnLogin.addConstraint(heightConst)
        }
        
        func setupUi() {
            view.backgroundColor = .white
            
            lblUserName.text = "Username"
            lblUserName.textColor = .black
            txtUsername.borderStyle = .roundedRect
            txtUsername.placeholder = "Username"
            
            lblPassword.text = "Password"
            lblPassword.textColor = .black
            txtPassword.borderStyle = .roundedRect
            txtPassword.placeholder = "Password"
            
            let usernameStack = UIStackView(arrangedSubviews: [lblUserName, txtUsername])
            usernameStack.axis = .vertical
            usernameStack.spacing = 4
            usernameStack.alignment = .fill
            usernameStack.distribution = .fill
            
            let passwordStack = UIStackView(arrangedSubviews: [lblPassword, txtPassword])
            passwordStack.axis = .vertical
            passwordStack.spacing = 4
            passwordStack.alignment = .fill
            passwordStack.distribution = .fill
            
            stackContaine = UIStackView(arrangedSubviews: [usernameStack, passwordStack])
            stackContaine.axis = .vertical
            stackContaine.spacing = 10
            stackContaine.alignment = .fill
            stackContaine.distribution = .fill
            
            self.view.addSubview(stackContaine)
            
            // Add Login button
            btnLogin.backgroundColor = .lightGray
            btnLogin.layer.cornerRadius = 6
            btnLogin.setTitle("Login", for: .normal)
            btnLogin.addTarget(self, action: #selector(actionLogin(_:)), for: .touchUpInside)
            
            self.view.addSubview(btnLogin)
        }


    @objc func actionLogin(_ sender: UIButton) {
        guard let username = txtUsername.text, username != "", let password = txtPassword.text, password != "" else {
            return
        }
        // "tharushid20+s231@gmail.com" "1qaz!QAZ"
        
        LoginVM.shared.login(userName: username, password: password) { (status, value) in
            DocumentVM.shared.getDocument { (data) in
                self.navigationController?.pushViewController(HomeVC(), animated: true)
            }
        }
    }
}

