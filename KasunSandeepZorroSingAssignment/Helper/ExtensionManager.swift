//
//  ExtensionManager.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit

extension FileManager {
    class func documentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
}

extension String {
    var kRead :Data {
        get {
            let data = try? Data(contentsOf: URL(fileURLWithPath: self))
            return data ?? Data()
        }
    }
}
