//
//  FileViewerManager.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit
import QuickLook

class FileViewerManager :QLPreviewController, QLPreviewControllerDataSource, QLPreviewControllerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    let navBarAppearince = UINavigationBar.appearance()

    var fileURL: URL?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.modalPresentationStyle = .fullScreen
        self.navigationController?.modalPresentationStyle = .fullScreen
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.black

        self.modalPresentationStyle = .fullScreen

        self.reloadData()
    }

    func openFileViewer(vc :UIViewController, link: String?) {
        self.delegate = self
        self.dataSource = self
        
        guard let linkURL = URL(string: link ?? "") else {
            return
        }

        self.fileURL = linkURL
        vc.present(self, animated: true)
    }

    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }

    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        guard let url = fileURL else {
            fatalError("Could not load file")
        }

        return url as QLPreviewItem
    }

    func previewControllerDidDismiss(_ controller: QLPreviewController) {
    }
    
}
