//
//  KeyChainManager.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit
import KeychainSwift

class KeyChainManager {
    static var shared: KeyChainManager = {
        let model = KeyChainManager()

        model.keyChain = KeychainSwift()
        model.keyChain.accessGroup = "kasun.KasunSandeepZorroSingAssignment"

        return model
    }()
    
    var keyChain: KeychainSwift!

    func set(value: String, key: String) {
        KeyChainManager.shared.keyChain.set(value, forKey: key)
    }
    
    func get(key: String) -> String? {
        KeyChainManager.shared.keyChain.get(key)
    }
}

extension String {
    static let accessToken = "ACCESS_TOKEN"
}
