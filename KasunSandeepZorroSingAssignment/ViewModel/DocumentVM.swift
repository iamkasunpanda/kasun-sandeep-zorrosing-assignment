//
//  DocumentVM.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit
import Alamofire

class DocumentVM: UIView {
    static var shared = DocumentVM()
    
    var directoryPath :String {
        get {
            return FileManager.documentsDir()
        }
    }
    var mediaDirectoryPath :String {
        get {
            return directoryPath + "document.pdf"
        }
    }

    func getDocument(completion :@escaping (_ data :Data?)->Void) {
        let urlString = "https://zswebworkflowqa.entrusttitle.net/api/v1/process/13850/document?objectId=workspace://SpacesStore/1401c5c7-5b65-4978-b1d4-3e8650616212;1.0n"
        let tocken = KeyChainManager.shared.get(key: .accessToken) ?? ""
        
        let headers: [String: String] = [
            "Authorization": "Bearer \(LoginVM.shared.token)"
        ]
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = URL(fileURLWithPath: self.mediaDirectoryPath)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers, to: destination)
        .validate().responseData { response in
            let data = self.mediaDirectoryPath.kRead
            completion(data)
        }
    }

}
