//
//  LoginVM.swift
//  KasunSandeepZorroSingAssignment
//
//  Created by Kasun Sandeep on 3/3/20.
//  Copyright © 2020 Kasun Sandeep. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

typealias actionHandler = (_ status: Bool, _ message: String) -> ()

class LoginVM: UIView {
    static var shared = LoginVM()
    
    var token = ""

    func login(userName: String, password: String, callback: @escaping actionHandler) {
        let urlString = "https://zswebuserqa.entrusttitle.net/api/Account/Login"
        let parameters: [String: Any] = [
            "UserName": userName,
            "Password": password,
            "ClientId": "123456",
            "ClientSecret": "abcdef",
            "DoNotSendActivationMail": false,
        ]

        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                if((response.result.value) != nil) {
                    let swiftyJsonVar = JSON(response.result.value!)
                    self.token = swiftyJsonVar["Data"]["ServiceToken"].stringValue
                    
                    KeyChainManager.shared.set(value: self.token, key: .accessToken)
                    callback(true, self.token)
                } else {
                    callback(false, "Login Fail")
                }
            }
    }

}
